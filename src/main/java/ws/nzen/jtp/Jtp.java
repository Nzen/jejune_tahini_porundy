
/* see ../../../LICENSE for release terms */

package ws.nzen.jtp;

import java.io.IOException;
import java.nio.file.InvalidPathException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.lang.SecurityException;
import java.util.LinkedList;
import java.util.List;

/** Adds the current folder name to a file.
 Which is to say, this is just for adding noise. */
public class Jtp
{
	public static void main( String[] args )
	{
		List<Path> toModify;
		if ( args == null || args.length < 1 )
		{
			toModify = getPathsOfFilesInWorkingDirectory();
		}
		else
		{
			/*
			check that they're valid
			bind these up to a list
			*/
			toModify = new LinkedList<>();
			int ind = 0;
			try
			{
				for ( String filename : args )
				{
					toModify.add( java.nio.file.Paths.get( filename ) );
					ind++;
				}
			}
			catch ( InvalidPathException ipe )
			{
				System.err.println( "Invalid file/path "+ args[ ind ]
						+" as "+ ipe );
			}
		}
		if ( toModify == null )
		{
			System.out.println( "JTP: didn't find valid files to modify" );
			return;
		}
		// it was this or a byte array
		List<String> here = new LinkedList<>();
		Path workingDir = null;
		String workingDirectory = "failed";
		try
		{
			workingDir = Paths.get( "" ).toAbsolutePath();
			workingDirectory = workingDir.getFileName().toString();
		}
		catch ( InvalidPathException ipe )
		{
			System.err.println( "curr dir "+ ipe );
		}
		here.add( workingDirectory );
		for ( Path currFile : toModify )
		{
			addToFile( here, copyHere( workingDir, currFile ) );
		}
	}

	static LinkedList<Path> getPathsOfFilesInWorkingDirectory()
	{
		return new LinkedList<Path>();
	}


	static Path copyHere( Path target, Path filename )
	{
		Path relocatedFile = null;
		try
		{
			relocatedFile = Files.copy( filename, target.resolve(
					filename.getFileName() ), java.nio.file
					.StandardCopyOption.REPLACE_EXISTING );
		}
		catch ( SecurityException | IOException ioe )
		// IllegalArgumentException UnsupportedOperationException
		{
			System.err.println( "jtp atf problem appending"+ ioe );
		}
		return relocatedFile;
	}


	static void addToFile( List<String> toAdd, Path filename )
	{
		if ( filename == null )
		{
			System.out.println( "jtp atf given an invalid path" );
			return;
		}
		try
		{
			if ( Files.isWritable( filename ) )
			{
				Files.write( filename, toAdd,
						java.nio.file.StandardOpenOption.APPEND );
			}
		}
		catch ( SecurityException | IOException ioe )
		// IllegalArgumentException UnsupportedOperationException
		{
			System.err.println( "jtp atf problem appending"+ ioe );
		}
	}
	
}






















