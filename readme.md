
### Jejune Tahini Porundy

A toy to preview using tup to compile a site. Run by specifying a remote (text) file to copy to the current directory. It will get a new line with the current directory. The tup demo is in the tup folder; run tup at its top level to copy f.txt to /one and /two (Porundy is 9 in Guarani, which is a Tupian language.)

Released under GPL v3 terms (better that I check off that merit badge with a throwaway project, than with one I expect to ever hope share &#11832;)
